<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('register');
    }

    public function kirim(Request $request){
        $namaDepan = $request['nama_depan']; 
        $namaBelakang = $request['nama_belakang']; 
        return view('welcome', compact('namaDepan','namaBelakang'));
    } 
}
